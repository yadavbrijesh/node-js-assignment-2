const path = require('path');

const express = require('express');

const CompController = require('../controllers/compnay');

const router = express.Router();

router.get('/company', CompController.getCompanies)

module.exports = router;
