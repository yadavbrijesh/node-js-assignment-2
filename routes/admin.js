const path = require('path');

const express = require('express');

const adminController = require('../controllers/admin');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-company', adminController.getAddProduct);

// /admin/products => GET
router.get('/companies', adminController.getProducts);

// /admin/add-product => POST
router.post('/add-company', adminController.postAddProduct);

router.get('/edit-product/:productId', adminController.getEditProduct);

router.post('/edit-company', adminController.postEditProduct);

router.post('/delete-company', adminController.postDeleteProduct);

router.get('/about-us', adminController.getAdd);

module.exports = router;
