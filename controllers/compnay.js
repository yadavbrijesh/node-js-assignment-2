const fs = require('fs');
const path = require('path');

const p = path.join(
  path.dirname(process.mainModule.filename),
  'data',
  'companies.json'
);

exports.getCompanies = (req, res, next) => {
     fs.readFile(p, (err,content)=>{
        if(!err){
           const file =  JSON.parse(content)
        //    console.log(file);
           res.render('company', {
            pageTitle: 'Companies',
            path: '/company',
            data: file
           })

        }
    })
  };