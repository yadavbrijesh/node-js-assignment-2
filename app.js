const path = require('path');
//core module

const express = require('express');
// 3rd party module

const bodyParser = require('body-parser');
// to parse req body

const errorController = require('./controllers/error');

const app = express();

app.set('view engine', 'ejs');
//setting template engines

app.set('views', 'views');
//default engine

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const company = require('./routes/company');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(company)
app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

app.listen(3000);
